This repo allows a Corsair K95 RGB keyboard to map its 'G' keys to AutoHotkey functions.  
Last tested on CUE2 version **2.21.67**  
Last tested on AutoHotkey version **1.1.25.01**


Note: To use Unicode symbols in an .ahk file, save the file with and encoding of "UTF-8 with BOM"



G keys are mapped as follows:
```
G#               - CUE Keystroke    - Virtual Code - Scan Code - Up/Down 
G1               - F13              - vk7C         - sc064     - u/d
G2               - F14              - vk7D         - sc065     - u/d
G3               - F15              - vk7E         - sc066     - u/d
G4               - F16              - vk7F         - sc067     - u/d
G5               - F17              - vk80         - sc068     - u/d
G6               - F18              - vk81         - sc069     - u/d
G7               - F19              - vk82         - sc06A     - u/d
G8               - F20              - vk83         - sc06B     - u/d
G9               - F21              - vk84         - sc06C     - u/d
G10              - Lang3            - vkFF         - sc078     - u
G11              - Lang2            - vkE9         - sc071     - u
G12              - Lang1            - vkFF         - sc072     - u
G13              - International6   - vkEA         - sc05C     - u/d
G14              - International5   - vkEB         - sc07B     - u/d
G15              - International4   - vkFF         - sc079     - u/d
G16              - International3   - vkFF         - sc07D     - u/d
G17              - International2   - vkFF         - sc070     - u/d
G18              - International1   - vkC1         - sc073     - u/d
Volume_Up        - Volume_Up        - vkAF         - sc130     - u/d
Volume_Down      - Volume_Down      - vkAE         - sc12E     - u/d
Volume_Mute      - Volume_Mute      - vkAD         - sc120     - u/d
Media_Stop       - Media_Stop       - vkB2         - sc124     - u/d
Media_Prev       - Media_Prev       - vkB1         - sc110     - u/d
Media_Play_Pause - Media_Play_Pause - vkB3         - sc122     - u/d
Media_Next       - Media_Next       - vkB0         - sc119     - u/d
```

Unused keys:
```
G#               - CUE Keystroke    - Virtual Code - Scan Code - Up/Down 
G1               - F22              - vk85         - sc06D     - u/d
G1               - F23              - vk86         - sc06E     - u/d
G1               - F24              - vk87         - sc076     - u/d
G1               - Lang4            - vkFF         - sc077     - u/d
G1               - Lang5            - vk87         - sc076     - u/d         F24 alias
G1               - Lang6            - undetected
G1               - Lang7            - undetected
G1               - Lang8            - undetected
G1               - Lang9            - undetected
G1               - Internation7     - undetected
G1               - Internation8     - undetected
G1               - Internation9     - undetected
G1               - NonUS_\          - vkE2         - sc056     - u/d         Not slash. Slash is vkDC - sc02B
G1               - NonUS-~          - vkDC         - sc02B     - u/d         \ alias
```


To use WMIC, the main machine needs to run in administrative powershell:
```
Set-NetConnectionProfile -NetworkCategory Private
winrm quickconfig
winrm set winrm/config/client '@{TrustedHosts="TARGET_COMPUTER_NAME"}'
```
Unknown: My need to add the user to the WMI settings by using Run -> compmgmt.msc -> Services and Applications -> WMI Control -> Right Click, Properties -> Security -> Security