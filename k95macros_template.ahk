#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
; Menu, Tray, Icon, C:\my_icon.ico

#SingleInstance Force

;STARTOFSCRIPT
SetTimer,UPDATEDSCRIPT,1000

UPDATEDSCRIPT:
FileGetAttrib,attribs,%A_ScriptFullPath%
IfInString,attribs,A
{
  FileSetAttrib,-A,%A_ScriptFullPath%
  SplashTextOn,,,Updated script,
  Sleep,500
  SplashTextOff
  Reload
}
Return
;ENDOFSCRIPT

F13:: ;G1
	
Return
F14:: ;G2
	
Return
F15:: ;G3
	
Return
F16:: ;G4
	
Return
F17:: ;G5
	
Return
F18:: ;G6
	
Return
F19:: ;G7

Return
F20:: ;G8
	
Return
F21:: ;G9
	
Return
sc078:: ;G10
	
Return
sc071:: ;G11
	
Return
sc072:: ;G12
	
Return
sc05C:: ;G13
	
Return
sc07B:: ;G14
	
Return
sc079:: ;G15
	
Return
sc07D:: ;G16
	
Return
sc070:: ;G17
	
Return
sc073:: ;G18
	
Return