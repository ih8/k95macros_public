#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
Menu, Tray, Icon, %A_ScriptDir%\ffp.ico
ConfigFile = %A_ScriptDir%\config.ini
#Include %A_ScriptDir%\Gdip_All.ahk

#SingleInstance Force

; G1 - G6, G18 are global hotkeys. G7 - G17 are program specific
#Include %A_ScriptDir%\programs\other.ahk
; #Include %A_ScriptDir%\programs\niche.ahk
#Include %A_ScriptDir%\programs\discord.ahk
; #Include %A_ScriptDir%\programs\explorer.ahk
#Include %A_ScriptDir%\programs\fortnite.ahk

; Send modifiers: ! alt, + shift, ^ ctrl, # win
; ==============================================================================
#If
F13:: ;G1
	IniRead, out, %ConfigFile%, UserData, primaryEmail
	Send, %out%
Return
F14:: ;G2
	IniRead, out, %ConfigFile%, Symbols, symbol0
	Send, %out%
Return
F15:: ;G3
	IniRead, out, %ConfigFile%, Phrases, phrase0
	Send, %out%
Return
; ==============================================================================
F16:: ;G4
	ReloadScript()
Return
F17:: ;G5
	IniRead, out, %ConfigFile%, Symbols, symbol1
	Send, %out%
Return
F18:: ;G6
	ToggleToF24() ;Located in other.ahk
Return
sc073:: ;vkC1 ;G18
	Send, ^!+{PrintScreen}
Return

; ==============================================================================
; Media buttons are also global

Volume_Mute:: ;sc03F

Return
Volume_Down:: ;sc12E

Return
Volume_Up:: ;sc130

Return
Media_Stop::

Return
*Media_Prev::

Return
*XButton2:: ;This button commonly mapped as 5th mouse button (Browser_Forward)
*Media_Play_Pause::

Return
*Media_Next::

Return

*XButton1:: ;This button commonly mapped as 4th mouse button (Browser_Back)
	ToggleToF24()
Return