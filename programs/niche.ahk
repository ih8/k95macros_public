;#IfWinActive ahk_exe program.exe
ConfigFile = %A_ScriptDir%\config.ini

SaveImageFromOnenote(){
	; Macro to export images from onenote to folder
	InputBox, name, Title
	path := "D:\heavenfall_ref\Clothing"
	FileCreateDir, %path%\%name%
	Send, ^{End}
	Sleep, 100
	Send, {Enter 2}
	Sleep, 100
	Send, ^{Home}
	Sleep, 100

	num = 1
	counter = 0
	while(true){
		WinGetTitle, Title, A
		IfInString, Title, OneNote 2016
		{
			numAsString := Format("{:04}", num)

			; if(num == 1){
			; 	FileCreateDir, D:\heavenfall_ref\anime\%name%
			; }

			Send, {Appskey}
			Sleep, 50
			Send, a
			Sleep, 100
			WinGetTitle, Title, A
			IfInString, Title, Save As
			{
				;FileAppend, D:\heavenfall_ref\anime\%name%\%name%_%numAsString%`n, B:\k95macros\ahk.out.txt
				Send, %path%\%name%\%name%_%numAsString%{enter}
				Sleep, 50
				num++
				counter = 0
			}Else{
				If(counter < 4){
					Send, {Esc}
					counter++
				} Else{
					Send, {Esc}
					Ding()
					Return
				}
			}
			Sleep, 50
			Send, {Right}
		}
		Sleep, 100
	}
}

TakeScreenshotAndSaveToDesktop(){
    Clipboard = 
    Send, #+s
    ClipWait, 10, 1

    ClipSaved := ClipboardAll
    If ClipSaved = 
    {
        ; MsgBox, 48, , Nothing Slected, 1
        Return
    }

    if(!pToken := Gdip_Startup()){
        MsgBox, 48, gdiplus error!, Gdiplus failed to start. Please ensure you have gdiplus on your system
        Return
    }

    pBitmap := Gdip_CreateBitmapFromClipboard()

    ; Get a pointer to the graphics of the bitmap, for use with drawing functions
    G := Gdip_GraphicsFromImage(pBitmap)

    Gdip_SaveBitmapToFile(pBitmap, "C:\Users\fat_f\Desktop\" A_Now ".png")

    Gdip_DisposeImage(pBitmap) ; The bitmap can be deleted
    Gdip_DeleteGraphics(G) ; The graphics may now be deleted
    Gdip_Shutdown(pToken) ; ...and gdi+ may now be shutdown
}