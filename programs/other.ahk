;#IfWinActive ahk_exe program.exe
ConfigFile = %A_ScriptDir%\config.ini
IniRead, primaryMicDeviceNumber, %ConfigFile%, SystemData, primaryMicDeviceNumber

ReloadScript(){
	SplashTextOn,,,Script reloaded,
	Sleep,500
	SplashTextOff
	Reload
}

LockWorkStation(){
	; Locks the computer and turns off monitors
	SendMessage, 0x112, 0xF170, 2,, Program Manager
	DllCall("LockWorkStation")
}

global G24Toggle := false
ToggleToF24(){
	; Macro to toggle F24
	; This is used to make push-to-talk a toggle-to-talk
		; Discord key code UNK135
	if(G24Toggle == false){
		Send {F24 down}
		G24Toggle := true
	}else{
		Send {F24 up}
		G24Toggle := false
	}
}

global OSMicMuteToggle := false
ToggleOSMicMute(){
	; Will mute microphone using ahk SoundSet.
	; This is akin to going into the Audio Recording Devices, selecting the device, 'Level' tab, and clicking the mute button
	; see here to find the mic's DeviceNumber: https://autohotkey.com/docs/commands/SoundSet.htm#Ex

	global
	if(OSMicMuteToggle == false){
		SoundSet, 1, MASTER, mute, %primaryMicDeviceNumber%
		SoundGet, master_mute, , mute, %primaryMicDeviceNumber%
		OSMicMuteToggle := true
		CubeColor := "FF4400"
	}else{
		SoundSet, 0, MASTER, mute, %primaryMicDeviceNumber%
		SoundGet, master_mute, , mute, %primaryMicDeviceNumber%
		OSMicMuteToggle := false
		CubeColor := "44FF44"
	}
	Gui, +ToolWindow -Caption +AlwaysOnTop
	Gui, Color, %CubeColor%
	Gui, Show, x0 y0 w10 h10 NoActivate
	Sleep, 500
	Gui, Destroy
}

global LClickSpamToggle := false
ToggleLClickSpam(){
	LClickSpamToggle := !LClickSpamToggle
    if(LClickSpamToggle) {
        SetTimer, LClickSpam, Off
    } else {
        SetTimer, LClickSpam, 13
    }
}

LClickSpam(){
	Send, {LButton}
}

; ==============================================================================
; For Debugging

Ding(){
	SoundPlay, %A_WinDir%\media\ding.wav
}

ShowMousePosition(){
	; Displays the position of the mouse cursor as a tooltip 
	CoordMode, ToolTip, Screen
	CoordMode, Mouse, Screen
	MouseGetPos, InitialX, InitialY
	tooltip %InitialX% %InitialY%, InitialX, InitialY
	clipboard = MouseMove, %InitialX%, %InitialY%
	SetTimer, RemoveToolTip, 2000
}

ShowWindowTitle(){
	; Displays the name of the active window as a tooltip
	CoordMode, ToolTip, Screen
	CoordMode, Mouse, Screen
	WinGetTitle, Title, A
	MouseGetPos, InitialX, InitialY
	tooltip %Title%, InitialX, InitialY
	clipboard == %Title%
	SetTimer, RemoveToolTip, 2000
}

RemoveToolTip(){
	SetTimer, RemoveToolTip, Off
	ToolTip
}

WriteInputToFile(){
	WinGetTitle, Title, A
	; clipboard == %Title%
	FileAppend, %Title%`n, B:\k95macros\ahk.out.txt
}