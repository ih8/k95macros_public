#IfWinActive ahk_exe explorer.exe
F19:: ;G7
    Rename480p()
Return
F20:: ;G8
	Rename720p()
Return
F21:: ;G9
	Rename1080p()
Return
; ==============================================================================
sc078:: ;vkFF ;G10
	;RenameFileSeries()
Return
sc071 up:: ;vkE9 ;G11

Return
sc072 up:: ;vkFF ;G12

Return
; ==============================================================================
sc05C up:: ;vkEA ;G13

Return
sc07B:: ;vkEB ;G14

Return
sc079:: ;vkFF ;G15

Return
; ==============================================================================
sc07D:: ;vkFF ;G16
	
Return
sc070:: ;vkFF ;G17
	
Return
; ==============================================================================

Rename480p(){
	WinGetTitle, Title, A
	IfInString, Title, H:\
	{
		Send, {F2}{Right} [480p]{Enter}
	}
}
Rename720p(){
	WinGetTitle, Title, A
	IfInString, Title, H:\
	{
		Send, {F2}{Right} [720p]{Enter}
	}
}
Rename1080p(){
	WinGetTitle, Title, A
	IfInString, Title, H:\
	{
		Send, {F2}{Right} [1080p]{Enter}
	}
}

RenameFileSeries(){
	WinGetTitle, Title, A
	IfInString, Title, H:\
	{
		static num = 1
		numAsString := Format("{:02}", num)
		num += 1
		Send, {F2}
		Sleep, 100
		Send, Title S01E%numAsString% [1080p]{enter}
		Sleep, 100
		Send, {Down}
	}
}